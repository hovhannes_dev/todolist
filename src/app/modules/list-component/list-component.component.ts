import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'list-component',
  templateUrl: './list-component.component.html',
  styleUrls: ['./list-component.component.scss']
})
export class ListComponent implements OnInit {

  /**
   * List Title
   * @type {string}
   */
  @Input() title: string;

  /**
   * List Title
   * @type {Array<string>}
   */
  @Input() items: string[] = [];

  /**
   * List update emitter
   * @type {EventEmitter<any>}
   */
  @Output() listUpdated: EventEmitter<string[]> = new EventEmitter<string[]>();

  /**
   * Item input NgModel
   * @type {string}
   */
  name: string = "";


  /**
   * Item input NgModel
   * @type {Object}
   */
  updateItem: any = {};

  constructor() { }

  ngOnInit() {
  }

  /*
  * Remove List item
  * */
  removeItem(index) {
    this.items.splice(index,1);
    this.listUpdated.emit(this.items);
  }

  /*
  * Add List item
  * */
  addItem(text) {
    if(!text.length) {
      alert("please enter text");
      return;
    }
    this.items.push(text);
    this.listUpdated.emit(this.items);
  }

  /*
  * Begin item update
  * */
  startUpdate(index,item) {
    this.updateItem = {
      index: index,
      text: item
    };
  }

  /*
  * Save List item
  * */
  saveItem() {
    this.items[this.updateItem.index] = this.updateItem.text;
    this.listUpdated.emit(this.items);
    this.updateItem = {};
  }

  /*
  * Cancel List item updating
  * */
  cancel(){
    this.updateItem = {}
  }

}
