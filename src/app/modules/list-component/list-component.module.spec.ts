import { ListComponentModule } from './list-component.module';

describe('ListComponentModule', () => {
  let listComponentModule: ListComponentModule;

  beforeEach(() => {
    listComponentModule = new ListComponentModule();
  });

  it('should create an instance', () => {
    expect(listComponentModule).toBeTruthy();
  });
});
