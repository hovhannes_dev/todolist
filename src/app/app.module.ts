import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {ListComponentModule} from './modules/list-component/list-component.module';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ListComponentModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
