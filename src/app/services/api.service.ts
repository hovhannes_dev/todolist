import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ApiUrls} from '../common/apiUrls';
import {ListInterface} from '../interfaces/list.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  /*
  * Get Group Id
  * @return {Promise<any>}
  * */
  getGroupId(): Promise<any> {
    return this.http.get(ApiUrls.getGroupId).toPromise()
  }

  /*
  * Get Group Id
  * @return {Promise<any>}
  * */
  getUserId(): Promise<any> {
    return this.http.get(ApiUrls.getUserId).toPromise()
  }

  /*
  * Get Group Id
  * @return {Promise<ListInterface>}
  * */
  getList(groupId: string, userId: string): Promise<ListInterface> {
    return this.http.get(ApiUrls.getList(groupId,userId)).toPromise() as Promise<ListInterface>
  }

  /*
  * Update list
  * @return {Promise<any>}
  * */
  updateList(groupId: string, userId: string, list: ListInterface): Promise<any> {
    return this.http.put(ApiUrls.getList(groupId,userId),list).toPromise()
  }

}
