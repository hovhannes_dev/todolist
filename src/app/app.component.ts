import {Component, OnInit} from '@angular/core';
import {ApiService} from './services/api.service';
import {ListInterface} from './interfaces/list.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  host:     {'[class.h-100]':'true','[class.d-block]':'true'}
})
export class AppComponent implements OnInit{

  /**
   * Header title
   * @type {string}
   */
  title = 'ProsCons';

  /**
   * User Id
   * @type {string}
   */
  userId: string;

  /**
   * Group Id
   * @type {string}
   */
  groupId: string;

  /**
   * List
   * @type {ListInterface}
   */
  list: ListInterface;

  /**
   * Loader state
   * @type {boolean}
   */
  loading: boolean;

  constructor(private apiService: ApiService) {}

  ngOnInit() {
    this.loading = true;
    const userPromise = this.apiService.getUserId().then(resp => this.userId = resp.userId);
    const groupPromise = this.apiService.getGroupId().then(resp => this.groupId = resp.groupId);
    Promise.all([userPromise,groupPromise]).then(() => {
      this.apiService.getList(this.groupId,this.userId)
        .then((resp) => {
          this.list = resp;
          this.loading = false;
        })
        .catch(e => console.error(e))
    }).catch(e => console.error(e))
  }

  /*
  * Update List
  * */
  updateList() {
    this.loading = true;
    this.apiService.updateList(this.groupId, this.userId, this.list).then(()=>{
      this.loading = false;
    }).catch(e => console.error(e))
  }
}
